require 'bundler'
Bundler.require
require 'yaml'

dbconfig = YAML.load(File.open('./db/config.yml'))['development']
ActiveRecord::Base.establish_connection(dbconfig)

# Loading models
Dir['./models/*.rb'].each { |file| require file }
