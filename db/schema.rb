# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_160_709_140_923) do
  create_table 'lastscrift_identities', force: :cascade do |t|
    t.text    'full_name'
    t.integer 'operator_id'
    t.string  'iban'
    t.string  'bic'
  end

  create_table 'operators', force: :cascade do |t|
    t.string   'email'
    t.string   'encrypted_password'
    t.string   'reset_password_token'
    t.string   'reset_password_token_sent_at'
    t.string   'sign_in_count'
    t.string   'current_sign_in_at'
    t.string   'current_sign_in_ip'
    t.string   'last_sign_in_ip'
    t.string   'confirmation_token'
    t.datetime 'confirmed_at'
    t.datetime 'confirmation_sent_at'
    t.boolean  'reauthenticate'
    t.string   'unconfirmed_email'
    t.string   'company'
    t.string   'phone'
    t.string   'name'
    t.string   'address_street'
    t.string   'address_city'
    t.string   'address_zip'
    t.datetime 'created_at',                   null: false
    t.datetime 'updated_at',                   null: false
  end

  create_table 'payments_transactions', force: :cascade do |t|
    t.float    'amount_paid'
    t.integer  'subscription_id'
    t.datetime 'created_at',      null: false
    t.datetime 'updated_at',      null: false
  end

  create_table 'secure_pricing_plans', force: :cascade do |t|
    t.string 'name'
    t.float  'amount'
  end

  create_table 'secure_subscriptions', force: :cascade do |t|
    t.integer  'pricing_plan_id'
    t.integer  'operator_id'
    t.datetime 'expires_on'
    t.datetime 'created_at',      null: false
    t.datetime 'updated_at',      null: false
  end
end
