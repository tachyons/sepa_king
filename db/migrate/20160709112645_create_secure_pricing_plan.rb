class CreateSecurePricingPlan < ActiveRecord::Migration
  def change
    create_table :secure_pricing_plans do |t|
      t.string :name
      t.float :amount
    end
  end
end
