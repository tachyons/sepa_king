class CreateLastscriftIdentity < ActiveRecord::Migration
  def change
    create_table :lastscrift_identities do |t|
      t.text :full_name
      t.references :operator
      t.string :iban
      t.string :bic
    end
  end
end
