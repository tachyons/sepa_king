class CreatePaymentsTransaction < ActiveRecord::Migration
  def change
    create_table :payments_transactions do |t|
      t.float :amount_paid
      t.references :subscription

      t.timestamps null: false
    end
  end
end
