class CreateOperator < ActiveRecord::Migration
  def change
    create_table :operators do |t|
      t.string :email
      t.string :encrypted_password
      t.string :reset_password_token
      t.string :reset_password_token_sent_at
      t.string :sign_in_count
      t.string :current_sign_in_at

      t.string :current_sign_in_ip
      t.string :last_sign_in_ip
      t.string :confirmation_token
      t.string :confirmed_at
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.boolean :reauthenticate
      t.string :unconfirmed_email
      t.string :company
      t.string :phone
      t.string :name
      t.string :address_street
      t.string :address_city
      t.string :address_zip

      t.datetime :confirmed_at
      t.timestamps null: false
    end
  end
end
