class CreateSecureSubscription < ActiveRecord::Migration
  def change
    create_table :secure_subscriptions do |t|
      t.references :pricing_plan
      t.references :operator
      t.datetime :expires_on

      t.timestamps null: false
    end
  end
end
