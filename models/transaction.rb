# == Schema Information
#
# Table name: payments_transactions
#
#  id              :integer          not null, primary key
#  amount_paid     :float
#  subscription_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Transaction < ActiveRecord::Base
  self.table_name_prefix = 'payments_'
  validates_uniqueness_of :subscription_id, scope: 'created_at' # TODO: convert this into date i!
  belongs_to :subscription
end
