# == Schema Information
#
# Table name: operators
#
#  id                           :integer          not null, primary key
#  email                        :string
#  encrypted_password           :string
#  reset_password_token         :string
#  reset_password_token_sent_at :string
#  sign_in_count                :string
#  current_sign_in_at           :string
#  current_sign_in_ip           :string
#  last_sign_in_ip              :string
#  confirmation_token           :string
#  confirmed_at                 :datetime
#  confirmation_sent_at         :datetime
#  reauthenticate               :boolean
#  unconfirmed_email            :string
#  company                      :string
#  phone                        :string
#  name                         :string
#  address_street               :string
#  address_city                 :string
#  address_zip                  :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#

class Operator < ActiveRecord::Base
  has_many :subscriptions
  has_one :lastscrift_identity
end
