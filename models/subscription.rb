# == Schema Information
#
# Table name: secure_subscriptions
#
#  id              :integer          not null, primary key
#  pricing_plan_id :integer
#  operator_id     :integer
#  expires_on      :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
require 'active_support/all'
class Subscription < ActiveRecord::Base
  self.table_name_prefix = 'secure_'

  validates :operator, presence: true
  validates :pricing_plan, presence: true

  belongs_to :operator
  belongs_to :pricing_plan
  has_one :transaction_record, class_name: 'Transaction'

  scope :at_date, ->(date) { where('DATE(created_at) = ? ', date) }
  scope :month_anniversary_at, ->(date) { where('DATE(created_at) = ? ', date - 1.month) }
  scope :yearly_anniversary_at, ->(date) { where('DATE(created_at) = ? ', date - 1.year) }
  scope :active_at, ->(date) { where('expires_on IS NULL or  DATE(expires_on) < ? ', date) }
  scope :of_type, ->(type) { joins(:pricing_plan).where(secure_pricing_plans: { name: type }) }

  def create_transaction
    Transaction.create(amount_paid: pricing_plan.amount, subscription: self)
  end

  def expired?
    expires_on.present? && expires_on < Date.today
  end
end
