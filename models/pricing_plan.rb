# == Schema Information
#
# Table name: secure_pricing_plans
#
#  id     :integer          not null, primary key
#  name   :string
#  amount :float
#

class PricingPlan < ActiveRecord::Base
  self.table_name_prefix = 'secure_'
  has_many :subscriptions
end
