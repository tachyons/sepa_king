# == Schema Information
#
# Table name: lastscrift_identities
#
#  id          :integer          not null, primary key
#  full_name   :text
#  operator_id :integer
#  iban        :string
#  bic         :string
#

class LastscriftIdentity < ActiveRecord::Base
  validates_with SEPA::IBANValidator
  validates_with SEPA::BICValidator
  belongs_to :operator
end
