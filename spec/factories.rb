FactoryGirl.define do
  factory :operator do
    first_name 'John Doe'
    email 'sample@exmple.com'
  end

  factory :subscription do
    pricing_plan
    operator
    expires_on nil
  end

  factory :pricing_plan do
    name 'daily'
    amount 4.95
  end

  factory :transaction do
    amount_paid 4.95
    subscription
  end

  factory :lastscrift_identity do
    full_name 'Jon Doe'
    operator
    iban 'DE21500500009876543210'
    bic 'SPUEDE2UXXX'
  end
end
