require 'rspec'
# require 'factory_girl'
Bundler.require
require 'support/factory_girl'

ENV['RACK_ENV'] = 'test'

require File.expand_path '../../app.rb', __FILE__

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
