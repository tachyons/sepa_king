require 'spec_helper'
RSpec.describe Subscription, type: :model do
  it 'is invalid without a operator' do
    expect(Subscription.new(operator: nil)).not_to be_valid
  end
end
