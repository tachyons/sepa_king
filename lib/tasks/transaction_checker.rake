require './lib/sepa_generator'
namespace :sepa do
  desc 'Daily process'
  task :daily_process, [:timestamp] => :environment do |_t, args|
    #args[:timestamp] ||= Date.today.to_s
    date = Date.parse(args[:timestamp] || (Date.today-1).to_s)
    sepa_generator = SepaGenerator.new(date)
    if sepa_generator.already_ran?
      puts "Script already ran for #{date}"
    else
      puts "Generating sepa for the date #{date}"
      SepaGenerator.new(date).to_xml
      SepaGenerator.new(date).send_email
    end
  end
end
