require './app'
class SepaGenerator
  EMAIL_RECIPIENT = "neo@codingarena.in"
  def initialize(date = (Date.today-1))
    @date = date
  end

  def to_xml
    subscriptions.each do |_subscription|
      ls_identity = _subscription.operator.lastscrift_identity
      sdd = SEPA::DirectDebit.new(
        name:     ls_identity.full_name,
        bic:        ls_identity.bic,
        iban:       ls_identity.iban,
        creditor_identifier: 'DE98ZZZ09999999999'
      )
      sdd.add_transaction(
        name:                      ls_identity.full_name,

        bic:                       ls_identity.bic,

        iban:                      ls_identity.iban,

        amount:                    _subscription.pricing_plan.amount.to_f,
        mandate_id:                'K-02-2011-12345',

        # Date
        mandate_date_of_signature: _subscription.created_at.to_date,

        local_instrument: 'CORE',

        sequence_type: 'OOFF',

        # Date
        requested_date: Date.new(2017, 9, 5)

      )
      _subscription.create_transaction
      sdd.to_xml
      File.open(xml_file_name, 'a+') do |file|
        file.write(sdd.to_xml)
      end
    end
  end

  def save_to_s3
    s3 = Aws::S3::Resource.new(region: 'us-west-2')
    obj = s3.bucket('bucket-name').object('key')
    obj.upload_file(xml_file_name)
  end

  def send_email
     mb_obj = Mailgun::MessageBuilder.new()
     mb_obj.from "Notifier <mailgun@sandbox3f7791fc9de44534a9d46f1930552cf5.mailgun.org>"
     mb_obj.add_recipient(:to, EMAIL_RECIPIENT );
     mb_obj.subject("Message from sepa generator");
     mb_obj.body_text("Sepa xml for #{@date} is attached with this mail");
     mb_obj.add_attachment(xml_file_name, xml_file_name);
     mg_client.send_message 'sandbox3f7791fc9de44534a9d46f1930552cf5.mailgun.org', mb_obj
  end

  def already_ran?
    File.file?(xml_file_name)
  end

  private

  def mg_client
    @mg_client = Mailgun::Client.new ENV["MAILGUN_API_KEY"]
  end

  def xml_file_name
    "sepa-#{@date}.xml"
  end

  def daily_pass_subscriptions
    Subscription.of_type('daily_pass').at_date(@date).active_at(@date)
  end

  def annual_subscriptions
    Subscription.of_type('annual').yearly_anniversary_at(@date).active_at(@date)
  end

  def monthly_subscriptions
    # TODO: month end
    Subscription.of_type('monthly').month_anniversary_at(@date).active_at(@date)
  end

  def subscriptions
    daily_pass_subscriptions + monthly_subscriptions + annual_subscriptions
  end
end
